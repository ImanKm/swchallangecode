package com.saitow.swchallangecode.network;


import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class WebServiceInitializer {
    private static Retrofit retrofit;
    private static OkHttpClient client;

    public static Retrofit initialize() {
        if (retrofit != null) {
            return retrofit;
        } else {
            return instantiateRetrofitWithBaseUrl();
        }
    }

    synchronized
    private static Retrofit instantiateRetrofitWithBaseUrl() {
        if (retrofit != null) {
            return retrofit;
        } else {
            GsonConverterFactory converterFactory = GsonConverterFactory.create(new GsonBuilder()
                    .setLenient()
                    .create());

            return retrofit = new Retrofit.Builder()
                    .baseUrl(UrlHandler.BASE_URL)
                    .addConverterFactory(converterFactory)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getHeader())
                    .build();
        }
    }

    private static OkHttpClient getHeader() {
        if (client == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .addNetworkInterceptor(new SupportInterceptor())
                    .build();
        }
        return client;
    }
}
