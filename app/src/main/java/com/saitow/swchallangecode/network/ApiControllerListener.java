package com.saitow.swchallangecode.network;

import retrofit2.Response;


public interface ApiControllerListener<T> {
    void onApiControllerResponse(T response);
    void onApiControllerError(Throwable throwable, Response<T> response);
}

