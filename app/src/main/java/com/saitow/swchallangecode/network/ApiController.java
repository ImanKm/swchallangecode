package com.saitow.swchallangecode.network;

import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.BicValidationModel;
import com.saitow.swchallangecode.models.IBANModel;
import com.saitow.swchallangecode.models.PostCodeModel;
import com.saitow.swchallangecode.models.PostCodeSendValidation;
import com.saitow.swchallangecode.models.SearchBicModel;
import com.saitow.swchallangecode.models.SearchBicSendModel;

import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiController {

    public static void searchBic(final SearchBicSendModel searchBicSendModel, final ApiControllerListener<BaseModel<SearchBicModel>> listener) {
        Call<BaseModel<SearchBicModel>> call = WebServiceInitializer.initialize().create(ApiCalls.class).searchBic(searchBicSendModel.getBlz(), searchBicSendModel.getCountryCode(), searchBicSendModel.getLocation(), searchBicSendModel.getBankname(), searchBicSendModel.getPage(), searchBicSendModel.getResOnPage());
        call.enqueue(new Callback<BaseModel<SearchBicModel>>() {
            @Override
            public void onResponse(@NonNull Call<BaseModel<SearchBicModel>> call, @NonNull Response<BaseModel<SearchBicModel>> response) {
                ResponseInterceptor.getResponseInterceptor(listener, response);
            }
            @Override
            public void onFailure(@NonNull Call<BaseModel<SearchBicModel>> call, @NonNull Throwable t) {
                ResponseInterceptor.getErrorInterceptor(listener, t);
            }
        });
    }

    public static void validateBic(final String bic, final ApiControllerListener<BaseModel<BicValidationModel>> listener) {

        Call<BaseModel<BicValidationModel>> call = WebServiceInitializer.initialize().create(ApiCalls.class).validateBic(bic);
        call.enqueue(new Callback<BaseModel<BicValidationModel>>() {
            @Override
            public void onResponse(@NonNull Call<BaseModel<BicValidationModel>> call, @NonNull Response<BaseModel<BicValidationModel>> response) {
                ResponseInterceptor.getResponseInterceptor(listener, response);
            }

            @Override
            public void onFailure(@NonNull Call<BaseModel<BicValidationModel>> call, @NonNull Throwable t) {
                ResponseInterceptor.getErrorInterceptor(listener, t);
            }
        });
    }

    public static void validateIban(final String iban, final ApiControllerListener<BaseModel<IBANModel>> listener) {
        Call<BaseModel<IBANModel>> call = WebServiceInitializer.initialize().create(ApiCalls.class).validateIban(iban);
        call.enqueue(new Callback<BaseModel<IBANModel>>() {
            @Override
            public void onResponse(@NonNull Call<BaseModel<IBANModel>> call, @NonNull Response<BaseModel<IBANModel>> response) {
                ResponseInterceptor.getResponseInterceptor(listener, response);
            }

            @Override
            public void onFailure(@NonNull Call<BaseModel<IBANModel>> call, @NonNull Throwable t) {
                ResponseInterceptor.getErrorInterceptor(listener, t);
            }
        });
    }


    public static void validatePostCode(final PostCodeSendValidation postCodeSendValidation, final ApiControllerListener<BaseModel<PostCodeModel>> listener) {

        Call<BaseModel<PostCodeModel>> call = WebServiceInitializer.initialize().create(ApiCalls.class).validatePostCode(postCodeSendValidation.getCountryCode(), postCodeSendValidation.getPostCode());
        call.enqueue(new Callback<BaseModel<PostCodeModel>>() {
            @Override
            public void onResponse(@NonNull Call<BaseModel<PostCodeModel>> call, @NonNull Response<BaseModel<PostCodeModel>> response) {
                ResponseInterceptor.getResponseInterceptor(listener, response);
            }

            @Override
            public void onFailure(@NonNull Call<BaseModel<PostCodeModel>> call, @NonNull Throwable t) {
                ResponseInterceptor.getErrorInterceptor(listener, t);
            }
        });
    }


}
