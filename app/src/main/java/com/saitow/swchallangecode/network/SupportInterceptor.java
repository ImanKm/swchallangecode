package com.saitow.swchallangecode.network;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class SupportInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder();
        requestBuilder.addHeader("Authorization", Credentials.basic("106901", "MTYzYmZkNjZiZmJiMTg2M2IwNjU2Nzk5NzI5OTVjNGY="));
        Request request = requestBuilder.build();
        Response response = chain.proceed(request);
        if (response.code() == 200) {
            //all responses
        }
        return response;
    }
}
