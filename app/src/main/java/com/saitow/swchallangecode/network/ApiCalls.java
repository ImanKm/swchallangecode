package com.saitow.swchallangecode.network;

import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.BicValidationModel;
import com.saitow.swchallangecode.models.IBANModel;
import com.saitow.swchallangecode.models.PostCodeModel;
import com.saitow.swchallangecode.models.SearchBicModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiCalls {

    @GET("searchBic")
    Call<BaseModel<SearchBicModel>> searchBic(@Query("blz") String blz, @Query("countryCode") String countryCode, @Query("location") String location, @Query("bankname") String bankname, @Query("page") String page, @Query("resOnPage") String resOnPage);

    @GET("validateBic")
    Call<BaseModel<BicValidationModel>> validateBic(@Query("bic") String bic);

    @GET("validateIban")
    Call<BaseModel<IBANModel>> validateIban(@Query("iban") String iban);

    @GET("validatePostCode")
    Call<BaseModel<PostCodeModel>> validatePostCode(@Query("countryCode") String countryCode, @Query("postCode") String postCode);

}
