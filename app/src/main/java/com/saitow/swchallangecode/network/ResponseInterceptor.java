package com.saitow.swchallangecode.network;

import com.saitow.swchallangecode.models.BaseModel;

import retrofit2.Response;

public class ResponseInterceptor {

    public static <T> void getResponseInterceptor(ApiControllerListener<BaseModel<T>> listener, Response<BaseModel<T>> response) {
        if (response == null) {
            listener.onApiControllerError(new Throwable(), response);
        } else if (response.body() == null) {
            listener.onApiControllerError(new Throwable(), response);
        } else if (response.body().getErrorCode() != null && !response.body().getErrorCode().equals("200")) {
            listener.onApiControllerError(new Throwable(), response);
        } else {
            listener.onApiControllerResponse(response.body());
        }
    }

    public static <T> void getErrorInterceptor(ApiControllerListener<BaseModel<T>> listener, Throwable t) {
        if (listener != null) {
            listener.onApiControllerError(t, null);
        }
    }

}
