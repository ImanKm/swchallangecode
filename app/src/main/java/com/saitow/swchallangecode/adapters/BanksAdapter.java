package com.saitow.swchallangecode.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.saitow.swchallangecode.R;
import com.saitow.swchallangecode.models.SearchBicModel;


public class BanksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    SearchBicModel searchBicModel;
    String nullMessage;

    public BanksAdapter( SearchBicModel searchBicModel,String nullMessage) {
        this.searchBicModel = searchBicModel;
        this.nullMessage = nullMessage;
    }

    @Override
    public int getItemViewType(int position) {
        if(searchBicModel == null){
            return 0;
        }else if (searchBicModel.getBicModelList()==null || searchBicModel.getBicModelList().size() == 0){
            if(position==0){
                return 1;
            }else{
                return 0;
            }
        }else{
            if(position==0){
                return 1;
            }else{
                return 2;
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(searchBicModel == null){
            return new NullHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.null_holder, parent, false));
        }else if (searchBicModel.getBicModelList()==null || searchBicModel.getBicModelList().size() == 0){
            if(i==0){
                return new NullHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.null_holder, parent, false));
            }else{
                return new ViewHolderBankSearch(LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_search_header_item, parent, false));
            }
        }else{
            if(i==1){
                return new ViewHolderBankSearch(LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_search_header_item, parent, false));
            }else{
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_item, parent, false));
            }
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if ( searchBicModel != null && ( searchBicModel.getBicModelList()==null || searchBicModel.getBicModelList().size() == 0)){
            if(holder.getAdapterPosition()==0){
                ((ViewHolderBankSearch)holder).bankNameTxt.setText(searchBicModel.getBankName());
                ((ViewHolderBankSearch)holder).locationTxt.setText(searchBicModel.getLocation());
                ((ViewHolderBankSearch)holder).countryCodeTxt.setText(searchBicModel.getCountryCode());
                ((ViewHolderBankSearch)holder).blzTxt.setText(searchBicModel.getBlz());
                ((ViewHolderBankSearch)holder).bicCodeTxt.setText(searchBicModel.getBic());
            }else{
                ((NullHolder)holder).messageTxt.setText(nullMessage);
            }
        }else if(searchBicModel != null) {
            if(holder.getAdapterPosition()==0){
                ((ViewHolderBankSearch)holder).bankNameTxt.setText(searchBicModel.getBankName());
                ((ViewHolderBankSearch)holder).locationTxt.setText(searchBicModel.getLocation());
                ((ViewHolderBankSearch)holder).countryCodeTxt.setText(searchBicModel.getCountryCode());
                ((ViewHolderBankSearch)holder).blzTxt.setText(searchBicModel.getBlz());
                ((ViewHolderBankSearch)holder).bicCodeTxt.setText(searchBicModel.getBic());
            }else{
                ((ViewHolder)holder).bankNameTxt.setText(searchBicModel.getBicModelList().get(holder.getAdapterPosition()).getBankName());
                ((ViewHolder)holder).locationTxt.setText(searchBicModel.getBicModelList().get(holder.getAdapterPosition()).getLocation());
                ((ViewHolder)holder).countryCodeTxt.setText(searchBicModel.getBicModelList().get(holder.getAdapterPosition()).getCountryCode());
                ((ViewHolder)holder).blzTxt.setText(searchBicModel.getBicModelList().get(holder.getAdapterPosition()).getBlz());
                ((ViewHolder)holder).bicCodeTxt.setText(searchBicModel.getBicModelList().get(holder.getAdapterPosition()).getBicCode());
                ((ViewHolder)holder).bicOtherTxt.setText(searchBicModel.getBicModelList().get(holder.getAdapterPosition()).getBicCodeOther());
            }
        }else{
            ((NullHolder)holder).messageTxt.setText(nullMessage);
        }
    }

    @Override
    public int getItemCount() {
        if(searchBicModel==null ) {
            return 1;
        }else if (searchBicModel.getBicModelList()==null || searchBicModel.getBicModelList().size() == 0){
            return 2;
        }else{
            return searchBicModel.getBicModelList().size()+1;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView bankNameTxt;
        TextView locationTxt;
        TextView countryCodeTxt;
        TextView blzTxt;
        TextView bicCodeTxt;
        TextView bicOtherTxt;
        public ViewHolder(View itemView) {
            super(itemView);
            bankNameTxt = (TextView)itemView.findViewById(R.id.bankNameTxt);
            locationTxt = (TextView)itemView.findViewById(R.id.locationTxt);
            countryCodeTxt = (TextView)itemView.findViewById(R.id.countryCodeTxt);
            blzTxt = (TextView)itemView.findViewById(R.id.blzTxt);
            bicCodeTxt = (TextView)itemView.findViewById(R.id.bicCodeTxt);
            bicOtherTxt = (TextView)itemView.findViewById(R.id.bicOtherTxt);
        }
    }

    class ViewHolderBankSearch extends RecyclerView.ViewHolder{
        TextView bankNameTxt;
        TextView locationTxt;
        TextView countryCodeTxt;
        TextView blzTxt;
        TextView bicCodeTxt;
        public ViewHolderBankSearch(View itemView) {
            super(itemView);
            bankNameTxt = (TextView)itemView.findViewById(R.id.bankNameTxt);
            locationTxt = (TextView)itemView.findViewById(R.id.locationTxt);
            countryCodeTxt = (TextView)itemView.findViewById(R.id.countryCodeTxt);
            blzTxt = (TextView)itemView.findViewById(R.id.blzTxt);
            bicCodeTxt = (TextView)itemView.findViewById(R.id.bicCodeTxt);
        }
    }


    public class NullHolder extends RecyclerView.ViewHolder {
        TextView messageTxt;
        public NullHolder(@NonNull View itemView) {
            super(itemView);
            messageTxt = (TextView)itemView.findViewById(R.id.messageTxt);
        }
    }

}