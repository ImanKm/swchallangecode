package com.saitow.swchallangecode.utiles;

public class ErrorHandler{

    public static String errorSet( String error ){
        if(error == null){
            return "An error has happened";
        }else if(error.equals("400")){
            return "Parameters missing. Please fill all fields";
        }else  if(error.equals("403")){
            return "Access Denied";
        }else{
            return "An error has happened";
        }
    }

}
