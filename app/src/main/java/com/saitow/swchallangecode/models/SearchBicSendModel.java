package com.saitow.swchallangecode.models;

public class SearchBicSendModel {

    String blz;
    String countryCode;
    String location;
    String bankname;
    String page;
    String resOnPage;

    public String getBlz() {
        return blz;
    }

    public void setBlz(String blz) {
        this.blz = blz;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getResOnPage() {
        return resOnPage;
    }

    public void setResOnPage(String resOnPage) {
        this.resOnPage = resOnPage;
    }
}
