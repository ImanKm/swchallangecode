package com.saitow.swchallangecode.models;

public class BicValidationModel {

    String bic;

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }
}
