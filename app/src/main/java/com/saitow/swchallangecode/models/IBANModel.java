package com.saitow.swchallangecode.models;

public class IBANModel {

    String iban;

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}
