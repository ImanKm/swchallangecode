package com.saitow.swchallangecode.models;

import java.util.List;

public class SearchBicModel {

    int page;
    int pageCount;
    String location;
    String bankName;
    String countryCode;
    String blz;
    String bic;
    List<BicModel> bicModelList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getBlz() {
        return blz;
    }

    public void setBlz(String blz) {
        this.blz = blz;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public List<BicModel> getBicModelList() {
        return bicModelList;
    }

    public void setBicModelList(List<BicModel> bicModelList) {
        this.bicModelList = bicModelList;
    }
}
