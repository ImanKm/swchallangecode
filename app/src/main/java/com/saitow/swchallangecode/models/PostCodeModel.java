package com.saitow.swchallangecode.models;

public class PostCodeModel {
    String postCode;

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
