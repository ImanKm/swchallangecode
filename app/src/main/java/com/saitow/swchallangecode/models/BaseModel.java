package com.saitow.swchallangecode.models;

public class BaseModel<T> {

    String code;
    String message;
    String errorCode;
    T data;

    public T getData() {
        return data;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
