package com.saitow.swchallangecode.viewmodels;

import io.reactivex.subjects.PublishSubject;

public abstract class BasicViewModel {


    /*This abstraction designed to prevent repeate action in it's child
     * However, using it's interfaces (abstracted methods) , the child classes will be enforced to
     * set proper methods
     * Note: each ViewModel should implement a message for erro handling
     * */


    protected PublishSubject<String> errorMessage = PublishSubject.create();

    public PublishSubject<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(PublishSubject<String> errorMessage) {
        this.errorMessage = errorMessage;
    }

}
