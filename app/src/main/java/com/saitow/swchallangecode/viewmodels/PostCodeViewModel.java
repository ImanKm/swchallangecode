package com.saitow.swchallangecode.viewmodels;

import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.PostCodeModel;
import com.saitow.swchallangecode.models.PostCodeSendValidation;
import com.saitow.swchallangecode.network.ApiController;
import com.saitow.swchallangecode.network.ApiControllerListener;
import com.saitow.swchallangecode.utiles.ErrorHandler;

import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;

public class PostCodeViewModel extends BasicViewModel {

    private PublishSubject<BaseModel<PostCodeModel>> postCodeModel = PublishSubject.create();

    private String selectedCountry = "";

    public PublishSubject<BaseModel<PostCodeModel>> getPostCodeModel() {
        return postCodeModel;
    }



    public void setPostCodeModel(PublishSubject<BaseModel<PostCodeModel>> postCodeModel) {
        this.postCodeModel = postCodeModel;
    }

    public String getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(String selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public void callValidatePostCode(final PostCodeSendValidation postCodeSendValidation) {
        ApiController.validatePostCode(postCodeSendValidation, new ApiControllerListener<BaseModel<PostCodeModel>>() {
            @Override
            public void onApiControllerResponse(BaseModel<PostCodeModel> response) {

                if (response.getCode().equals("ERR_GEO_INVALID_POST_CODE")) {

                    response.setMessage("Your post code is not valid (" + response.getData().getPostCode() + ")");

                } else if (response.getCode().equals("ERR_GEO_INVALID_COUNTRY_CODE")) {

                    response.setMessage("The country code is not valid (" + response.getData().getPostCode() + ")");

                } else {

                    response.setMessage("Your post code is valid (" + response.getData().getPostCode() + ")");

                }

                postCodeModel.onNext(response);

            }

            @Override
            public void onApiControllerError(Throwable throwable, Response<BaseModel<PostCodeModel>> response) {
                if(response != null && response.body() != null && response.body().getErrorCode() != null && response.body().getErrorCode() != null){
                    errorMessage.onNext(ErrorHandler.errorSet(response.body().getCode()));
                }else{
                    errorMessage.onNext("An error has happened");
                }
            }
        });
    }


}
