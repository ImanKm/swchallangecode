package com.saitow.swchallangecode.viewmodels;

import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.BicValidationModel;
import com.saitow.swchallangecode.models.IBANModel;
import com.saitow.swchallangecode.network.ApiController;
import com.saitow.swchallangecode.network.ApiControllerListener;
import com.saitow.swchallangecode.utiles.ErrorHandler;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;

public class TransferValidationViewModel extends BasicViewModel {

    private PublishSubject<BaseModel<BicValidationModel>> bicValidationModel = PublishSubject.create();
    private PublishSubject<BaseModel<IBANModel>> iBANModel = PublishSubject.create();

    public PublishSubject<BaseModel<BicValidationModel>> getBicValidationModel() {
        return bicValidationModel;
    }

    public void setBicValidationModel(PublishSubject<BaseModel<BicValidationModel>> bicValidationModel) {
        this.bicValidationModel = bicValidationModel;
    }

    public PublishSubject<BaseModel<IBANModel>> getiBANModel() {
        return iBANModel;
    }

    public void setiBANModel(PublishSubject<BaseModel<IBANModel>> iBANModel) {
        this.iBANModel = iBANModel;
    }

    public void callValidateBic(String bic) {
        ApiController.validateBic(bic, new ApiControllerListener<BaseModel<BicValidationModel>>() {
            @Override
            public void onApiControllerResponse(BaseModel<BicValidationModel> response) {
                if (response.getCode().equals("ERR_FINANCIAL_INVALID_BIC")) {
                    response.setMessage("The BIC code is not valid" + " (" + response.getData().getBic() + ")");
                } else {
                    response.setMessage("The BIC code is valid" + " (" + response.getData().getBic() + ")");
                }

                bicValidationModel.onNext(response);
            }

            @Override
            public void onApiControllerError(Throwable throwable, Response<BaseModel<BicValidationModel>> response) {
            }
        });
    }

    public void callValidateIban(String iban) {
        ApiController.validateIban(iban, new ApiControllerListener<BaseModel<IBANModel>>() {
            @Override
            public void onApiControllerResponse(BaseModel<IBANModel> response) {
                if (response.getCode().equals("ERR_FINANCIAL_INVALID_IBAN")) {
                    response.setMessage("The IBAN code is not valid" + " (" + response.getData().getIban() + ")");
                } else {
                    response.setMessage("The IBAN code is valid" + " (" + response.getData().getIban() + ")");
                }
                iBANModel.onNext(response);
            }

            @Override
            public void onApiControllerError(Throwable throwable, Response<BaseModel<IBANModel>> response) {
                if(response != null && response.body() != null && response.body().getErrorCode() != null && response.body().getErrorCode() != null){
                    errorMessage.onNext(ErrorHandler.errorSet(response.body().getCode()));
                }else{
                    errorMessage.onNext("An error has happened");
                }
            }
        });
    }

}
