package com.saitow.swchallangecode.viewmodels;

import android.util.Log;

import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.SearchBicModel;
import com.saitow.swchallangecode.models.SearchBicSendModel;
import com.saitow.swchallangecode.network.ApiController;
import com.saitow.swchallangecode.network.ApiControllerListener;
import com.saitow.swchallangecode.utiles.ErrorHandler;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;

public class BankSearchViewModel extends BasicViewModel {

    private String selectedCountry = "";

    private PublishSubject<BaseModel<SearchBicModel>> searchBicModel = PublishSubject.create();

    public String getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(String selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public Observable<BaseModel<SearchBicModel>> getSearchBicModel() {
        return searchBicModel;
    }

    public void setSearchBicModel(PublishSubject<BaseModel<SearchBicModel>> searchBicModel) {
        this.searchBicModel = searchBicModel;
    }

    public void callBankSearch(SearchBicSendModel searchBicSendModel){
        ApiController.searchBic( searchBicSendModel, new ApiControllerListener<BaseModel<SearchBicModel>>() {
            @Override
            public void onApiControllerResponse(BaseModel<SearchBicModel> response) {
                if (response.getCode().equals("ERR_RESTRICTED_ACCESS")) {
                    response.setMessage("The access to this endpoint is not allowed for your user");
                } else {
                    response.setMessage("No Information found. Do a query :)");
                }
                searchBicModel.onNext(response);
            }
            @Override
            public void onApiControllerError(Throwable throwable, Response<BaseModel<SearchBicModel>> response) {
                if(response != null && response.body() != null && response.body().getErrorCode() != null && response.body().getErrorCode() != null){
                    errorMessage.onNext(ErrorHandler.errorSet(response.body().getCode()));
                }else{
                    errorMessage.onNext("An error has happened");
                }
            }
        });
    }

}
