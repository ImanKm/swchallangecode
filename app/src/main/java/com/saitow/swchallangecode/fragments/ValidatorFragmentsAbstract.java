package com.saitow.swchallangecode.fragments;

import android.app.Dialog;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public abstract class ValidatorFragmentsAbstract extends Fragment {


    /*This abstraction designed to prevent repeate action in it's child
    * However, using it's interfaces (abstracted methods) , the child classes will be enforced to
    * set proper methods
    * Note: each fragment has it's own dialog to search.
    * */

    protected Dialog dialog;
    protected Disposable disposable;

    private static final String TAG = "In Fragment Abstraction";

    public abstract void showDialog();

    protected abstract void setObserver();

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    protected Observer<String> getMessageObserver() {
        return new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }
            @Override
            public void onNext(final String s) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(),s,Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }
            @Override
            public void onComplete() {

            }
        };
    }

 }
