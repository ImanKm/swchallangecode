package com.saitow.swchallangecode.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.saitow.swchallangecode.R;
import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.BicValidationModel;
import com.saitow.swchallangecode.models.IBANModel;
import com.saitow.swchallangecode.viewmodels.TransferValidationViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TransferValidationFragment extends ValidatorFragmentsAbstract {

    TextView bicInfoTxt;
    TextView ibanInfoTxt;

    TransferValidationViewModel transferValidationViewModel = new TransferValidationViewModel();

    private static String TAG = "TransferValidationFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.transfer_validation_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bicInfoTxt = getActivity().findViewById(R.id.bicInfoTxt);
        ibanInfoTxt = getActivity().findViewById(R.id.ibanInfoTxt);

       setObserver();

    }

    @Override
    protected void setObserver() {

        transferValidationViewModel.getBicValidationModel()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getBicObserver());

        transferValidationViewModel.getiBANModel()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getIbanObserver());

        transferValidationViewModel.getErrorMessage()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getMessageObserver());

    }

    private Observer<BaseModel<IBANModel>> getIbanObserver() {
        return new Observer<BaseModel<IBANModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(final BaseModel<IBANModel> s) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ibanInfoTxt.setText(s.getMessage());
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e(TAG, "onComplite");
            }
        };
    }





    private Observer<BaseModel<BicValidationModel>> getBicObserver() {
        return new Observer<BaseModel<BicValidationModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(final BaseModel<BicValidationModel> s) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bicInfoTxt.setText(s.getMessage());
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e(TAG, "onComplite");
            }
        };
    }

    @Override
    public void showDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.trasfer_validation_dialog);

        Button queryBtn = dialog.findViewById(R.id.queryBtn);
        final EditText bicEditText = dialog.findViewById(R.id.bicEditText);
        final EditText ibanEditText = dialog.findViewById(R.id.ibanEditText);

        queryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bicEditText.getText().toString().equals("")) {
                    transferValidationViewModel.callValidateBic(bicEditText.getText().toString());
                }
                if (!ibanEditText.getText().toString().equals("")) {
                    transferValidationViewModel.callValidateIban(ibanEditText.getText().toString());
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }




}
