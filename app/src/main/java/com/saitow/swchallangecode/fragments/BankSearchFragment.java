package com.saitow.swchallangecode.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.saitow.swchallangecode.R;
import com.saitow.swchallangecode.adapters.BanksAdapter;
import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.SearchBicModel;
import com.saitow.swchallangecode.models.SearchBicSendModel;
import com.saitow.swchallangecode.utiles.CountriesIsoCode;
import com.saitow.swchallangecode.viewmodels.BankSearchViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BankSearchFragment extends ValidatorFragmentsAbstract {

    RecyclerView banksRecycleView;
    BanksAdapter banksAdapter;

    BankSearchViewModel bankSearchViewModel = new BankSearchViewModel();

    private static String TAG = "BankSearchFragment" ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bank_search_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        banksRecycleView = getActivity().findViewById(R.id.banksRecycleView);
        banksRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        banksAdapter = new BanksAdapter(null, getResources().getString(R.string.no_data));
        banksRecycleView.setAdapter(banksAdapter);

        setObserver();
    }

    @Override
    protected void setObserver() {
        bankSearchViewModel.getSearchBicModel()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver());

        bankSearchViewModel.getErrorMessage()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getMessageObserver());
    }

    private Observer<BaseModel<SearchBicModel>> getObserver() {
        return new Observer<BaseModel<SearchBicModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }
            @Override
            public void onNext(final BaseModel<SearchBicModel> s) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        banksAdapter = new BanksAdapter( s.getData(), s.getMessage());
                        banksRecycleView.setAdapter(banksAdapter);
                    }
                });
            }
            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }
            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void showDialog() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bank_search_dialog);
        Button queryBtn = dialog.findViewById(R.id.queryBtn);

        final EditText blzEditText = dialog.findViewById(R.id.blzEditText);
        final EditText locationEditText = dialog.findViewById(R.id.locationEditText);
        final EditText bankNameEditText = dialog.findViewById(R.id.bankNameEditText);

        final Spinner countrySpinner = dialog.findViewById(R.id.countrySpinner);
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, CountriesIsoCode.getCountries());
        countrySpinner.setAdapter(arrayAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankSearchViewModel.setSelectedCountry(CountriesIsoCode.getCountries()[position]);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        queryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //the model to pass data is better. Over than 2 or 3 parametters not allawded(Clean Code)
                SearchBicSendModel searchBicSendModel = new SearchBicSendModel();
                searchBicSendModel.setBlz(blzEditText.getText().toString());
                searchBicSendModel.setCountryCode(bankSearchViewModel.getSelectedCountry());
                searchBicSendModel.setLocation(locationEditText.getText().toString());
                searchBicSendModel.setBankname(bankNameEditText.getText().toString());
                searchBicSendModel.setResOnPage("20");
                searchBicSendModel.setPage("1");
                bankSearchViewModel.callBankSearch(searchBicSendModel);
                dialog.dismiss();

            }
        });
        dialog.show();
    }


}
