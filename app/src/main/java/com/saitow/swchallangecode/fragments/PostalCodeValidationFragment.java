package com.saitow.swchallangecode.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.saitow.swchallangecode.R;
import com.saitow.swchallangecode.models.BaseModel;
import com.saitow.swchallangecode.models.PostCodeModel;
import com.saitow.swchallangecode.models.PostCodeSendValidation;
import com.saitow.swchallangecode.utiles.CountriesIsoCode;
import com.saitow.swchallangecode.viewmodels.PostCodeViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PostalCodeValidationFragment extends ValidatorFragmentsAbstract {

    TextView postCodeMessageTxt;
    PostCodeViewModel postCodeViewModel = new PostCodeViewModel();

    private static String TAG = "PostalCodeValidationFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.postcode_validation_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        postCodeMessageTxt = getActivity().findViewById(R.id.postCodeMessageTxt);
        setObserver();
    }

    @Override
    protected void setObserver() {
        postCodeViewModel.getPostCodeModel()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver());

        postCodeViewModel.getErrorMessage()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(getMessageObserver());

    }

    private Observer<BaseModel<PostCodeModel>> getObserver() {
        return new Observer<BaseModel<PostCodeModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(final BaseModel<PostCodeModel> s) {

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        postCodeMessageTxt.setText(s.getMessage());
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }



    @Override
    public void showDialog() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.post_code_validation_dialog);

        Button queryBtn = dialog.findViewById(R.id.queryBtn);
        final EditText postCodeEditText = dialog.findViewById(R.id.postCodeEditText);

        final Spinner countrySpinner = dialog.findViewById(R.id.countrySpinner);
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, CountriesIsoCode.getCountries());
        countrySpinner.setAdapter(arrayAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                postCodeViewModel.setSelectedCountry(CountriesIsoCode.getCountries()[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        queryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!postCodeViewModel.getSelectedCountry().equals("") && !postCodeEditText.getText().toString().equals("")) {
                    PostCodeSendValidation postCodeSendValidation = new PostCodeSendValidation();
                    postCodeSendValidation.setCountryCode(postCodeViewModel.getSelectedCountry());
                    postCodeSendValidation.setPostCode(postCodeEditText.getText().toString());
                    postCodeViewModel.callValidatePostCode(postCodeSendValidation);
                    dialog.dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_LONG).show();
                }
            }
        });
        dialog.show();
    }


}
