package com.saitow.swchallangecode.activities;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.saitow.swchallangecode.R;
import com.saitow.swchallangecode.adapters.TabsAdapter;
import com.saitow.swchallangecode.fragments.BankSearchFragment;
import com.saitow.swchallangecode.fragments.PostalCodeValidationFragment;
import com.saitow.swchallangecode.fragments.TransferValidationFragment;
import com.saitow.swchallangecode.fragments.ValidatorFragmentsAbstract;
import com.saitow.swchallangecode.interfaces.MainActivityInterface;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,MainActivityInterface {

    private TabsAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        setTabAdapter();

    }


    //set tabbar adapter by three different fragmetn.
    // for next itiration, it's better to move const strings to another proper place
    @Override
    public void setTabAdapter() {

        int[] tabIcons = {
                R.drawable.bank_icon,
                R.drawable.card_icon,
                R.drawable.location_icon
        };

        adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(new BankSearchFragment(), "Bank Search");
        adapter.addFragment(new TransferValidationFragment(), "Transfer");
        adapter.addFragment(new PostalCodeValidationFragment(), "Post Code");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

    }


    //it comes from main activity layout directly. it's belong to float action button
    @Override
    public void onClick(View v) {
        ((ValidatorFragmentsAbstract) adapter.getItem(tabLayout.getSelectedTabPosition())).showDialog();
    }


}
